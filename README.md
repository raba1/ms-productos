# ms-productos

1.Acceder al repositorio: 

	https://gitlab.com/raba1/ms-productos
	
2.Clonar Proyecto
	git clone https://gitlab.com/raba1/ms-productos.git

3.Ejecutar en consola el siguiente comando:

	gradle build

4.Para correrlo ejecutar lo siguiente:

	gradle bootRun

5.Los recursos estan en la collection "Productos.postman_collection.json" que esta en la carpeta resources del proyecto.

6.Tecnologias utilizadas

Java 8.
Persistencia: H2.
Mockito.

	