package com.prueba.msproductos.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import com.prueba.msproductos.dto.ProductoRequestDto;
import com.prueba.msproductos.dto.ProductoResponseDto;
import com.prueba.msproductos.entity.Producto;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class ProductoMapperTest {
	
	@InjectMocks
	private ProductoMapper productoMapper;
	
	@Mock
	private ModelMapper modelMapper;
	
	@Test
	public void convertProductoToResponseDTO() {
		Producto producto = new Producto();
		producto.setBrand("Marca 1");
		producto.setListUrls(null);
		producto.setName("Name 1");
		producto.setPrice(new BigDecimal(100000));
		producto.setSize("30");
		producto.setSku("FAL-1000000");
		producto.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		ProductoResponseDto responseEsperado = new ProductoResponseDto();
		responseEsperado.setBrand("Marca 1");
		responseEsperado.setListUrls(null);
		responseEsperado.setName("Name 1");
		responseEsperado.setPrice(new BigDecimal(100000));
		responseEsperado.setSize("30");
		responseEsperado.setSku("FAL-1000000");
		responseEsperado.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(responseEsperado);
		
		ProductoResponseDto resp = productoMapper.convertProductoToResponseDTO(producto);
		assertNotNull(resp);
		assertEquals(responseEsperado, resp, "El objeto obtenido, no corresponde con el esperado");
		
	}
	
	@Test
	public void testConvertRequestToProductoDTO() {
		
		ProductoRequestDto request = new ProductoRequestDto();
		request.setBrand("Marca 1");
		request.setListUrls(null);
		request.setName("Name 1");
		request.setPrice(new BigDecimal(100000));
		request.setSize("30");
		request.setSku("FAL-1000000");
		request.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Producto productoEsperado = new Producto();
		productoEsperado.setBrand("Marca 1");
		productoEsperado.setListUrls(null);
		productoEsperado.setName("Name 1");
		productoEsperado.setPrice(new BigDecimal(100000));
		productoEsperado.setSize("30");
		productoEsperado.setSku("FAL-1000000");
		productoEsperado.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(productoEsperado);
		
		Producto resp = productoMapper.convertRequestToProductoDTO(request);
		assertNotNull(resp);
		assertEquals(productoEsperado, resp, "El objeto obtenido, no corresponde con el esperado");
		
	}

}
