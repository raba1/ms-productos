package com.prueba.msproductos.serviceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import com.prueba.msproductos.dto.ProductoRequestDto;
import com.prueba.msproductos.dto.ProductoResponseDto;
import com.prueba.msproductos.entity.Producto;
import com.prueba.msproductos.exception.ResourceNotFoundException;
import com.prueba.msproductos.mapper.ProductoMapper;
import com.prueba.msproductos.repository.IProductoRepository;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class ProductosServiceImplTest {
	
	@InjectMocks
	private ProductosServiceImpl productosServiceImpl;
	
	@Mock
	private IProductoRepository repository;
	
	@Mock
	private ModelMapper modelMapper;
	
	@Mock
	private ProductoMapper productoMapper;

	@Test
	public void testSave() throws ResourceNotFoundException {
		ArrayList<String> urls = new ArrayList<>(
				Arrays.asList("https://falabella.scene7.com/is/image/Falabella/8406270_1",
						"https://falabella.scene7.com/is/image/Falabella/8406270_2",
						"https://falabella.scene7.com/is/image/Falabella/8406270_3"));

		ProductoRequestDto request = new ProductoRequestDto();
		request.setBrand("Marca 1");
		request.setListUrls(urls);
		request.setName("Name 1");
		request.setPrice(new BigDecimal(100000));
		request.setSize("30");
		request.setSku("FAL-1000000");
		request.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Producto productoGuardado = new Producto();
		productoGuardado.setBrand("Marca 1");
		productoGuardado.setListUrls(urls);
		productoGuardado.setName("Name 1");
		productoGuardado.setPrice(new BigDecimal(100000));
		productoGuardado.setSize("30");
		productoGuardado.setSku("FAL-1000000");
		productoGuardado.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		ProductoResponseDto responseEsperado = new ProductoResponseDto();
		responseEsperado.setBrand("Marca 1");
		responseEsperado.setListUrls(urls);
		responseEsperado.setName("Name 1");
		responseEsperado.setPrice(new BigDecimal(100000));
		responseEsperado.setSize("30");
		responseEsperado.setSku("FAL-1000000");
		responseEsperado.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Mockito.when(repository.findBySku(Mockito.anyString())).thenReturn(null);
		Mockito.when(repository.save(Mockito.any())).thenReturn(productoGuardado);
		Mockito.when(productoMapper.convertProductoToResponseDTO(Mockito.any())).thenReturn(responseEsperado);
		
		ProductoResponseDto resp = productosServiceImpl.save(request);
		assertNotNull(resp);
		assertEquals(responseEsperado, resp, "El objeto obtenido luego de guardar el producto, no corresponde con el esperado");
		
	}
	
	@Test
	public void testSaveException() {
		
		ProductoRequestDto request = new ProductoRequestDto();
		request.setBrand("Marca 1");
		request.setListUrls(null);
		request.setName("Name 1");
		request.setPrice(new BigDecimal(100000));
		request.setSize("30");
		request.setSku("FAL-1000000");
		request.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Producto producto = new Producto();
		producto.setBrand("Marca 1");
		producto.setListUrls(null);
		producto.setName("Name 1");
		producto.setPrice(new BigDecimal(100000));
		producto.setSize("30");
		producto.setSku("FAL-1000000");
		producto.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Mockito.when(repository.findBySku(Mockito.anyString())).thenReturn(producto);
		
		try {
			 productosServiceImpl.save(request);
		} catch (ResourceNotFoundException e) {
			assertEquals("El Sku del producto ya existe", e.getMessage());
		}
		
	}
	
	
	
	@Test
	public void testGetAllProductos() throws ResourceNotFoundException {
		
		Producto productoMock1 = new Producto();
		productoMock1.setBrand("Marca 1");
		productoMock1.setListUrls(null);
		productoMock1.setName("Name 1");
		productoMock1.setPrice(new BigDecimal(100000));
		productoMock1.setSize("30");
		productoMock1.setSku("FAL-1000000");
		productoMock1.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Producto productoMock2 = new Producto();
		productoMock2.setBrand("Marca 2");
		productoMock2.setListUrls(null);
		productoMock2.setName("Name 2");
		productoMock2.setPrice(new BigDecimal(200000));
		productoMock2.setSize("M");
		productoMock2.setSku("FAL-2000000");
		productoMock2.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_5");
		
		ArrayList<Producto> productos = new ArrayList<Producto>();
		productos.add(productoMock1);
		productos.add(productoMock2);
		
		Mockito.when(repository.findAll()).thenReturn(productos);
		Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(productos);
		
		List<ProductoResponseDto> listaResp = productosServiceImpl.getAllProductos();
		assertNotNull(listaResp);
		assertEquals(2, listaResp.size(), "La cantidad de productos, no corresponde con el esperado");
		assertEquals(productos, listaResp, "El objeto obtenido con los productos, no corresponde con el esperado");
		
	}
	
	@Test
	public void testGetAllProductosException() {
		
		Mockito.when(repository.findAll()).thenReturn(new ArrayList<Producto>());
		try {
			 productosServiceImpl.getAllProductos();
		} catch (ResourceNotFoundException e) {
			assertEquals("No existen productos", e.getMessage());
		}
		
	}
	
	@Test
	public void TestGetProducto() throws ResourceNotFoundException {
		
		Producto producto = new Producto();
		producto.setBrand("Marca 1");
		producto.setListUrls(null);
		producto.setName("Name 1");
		producto.setPrice(new BigDecimal(100000));
		producto.setSize("30");
		producto.setSku("FAL-1000000");
		producto.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		ProductoResponseDto responseEsperado = new ProductoResponseDto();
		responseEsperado.setBrand("Marca 1");
		responseEsperado.setListUrls(null);
		responseEsperado.setName("Name 1");
		responseEsperado.setPrice(new BigDecimal(100000));
		responseEsperado.setSize("30");
		responseEsperado.setSku("FAL-1000000");
		responseEsperado.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Mockito.when(repository.findBySku(Mockito.anyString())).thenReturn(producto);
		Mockito.when(productoMapper.convertProductoToResponseDTO(Mockito.any())).thenReturn(responseEsperado);
		
		ProductoResponseDto resp = productosServiceImpl.getProducto("FAL-1000000");
		assertNotNull(resp);
		assertEquals(responseEsperado, resp, "El producto obtenido, no corresponde con el esperado");
		
	}
	
	@Test
	public void TestGetProductoException() {
		
		Mockito.when(repository.findBySku(Mockito.anyString())).thenReturn(null);
		
		try {
			productosServiceImpl.getProducto("FAL-1000000");
		} catch (ResourceNotFoundException e) {
			assertEquals("El producto no existe", e.getMessage());
		}
		
	}
	
	@Test
	public void testDeleteProducto() throws ResourceNotFoundException {
		
		Producto producto = new Producto();
		producto.setBrand("Marca 1");
		producto.setListUrls(null);
		producto.setName("Name 1");
		producto.setPrice(new BigDecimal(100000));
		producto.setSize("30");
		producto.setSku("FAL-1000000");
		producto.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Mockito.when(repository.findBySku(Mockito.anyString())).thenReturn(producto);
		doNothing().when(repository).delete(Mockito.any());
		
		productosServiceImpl.deleteProducto("FAL-1000000");
		
	}
	
	@Test
	public void testDeleteProductoException() {
		
		Mockito.when(repository.findBySku(Mockito.anyString())).thenReturn(null);
		
		try {
			productosServiceImpl.deleteProducto("FAL-1000000");
		} catch (ResourceNotFoundException e) {
			assertEquals("El producto no existe", e.getMessage());
		}
		
	}
	
	@Test
	public void testUpdateProducto() throws ResourceNotFoundException {
		
		ArrayList<String> urls = new ArrayList<>(
				Arrays.asList("https://falabella.scene7.com/is/image/Falabella/8406270_1",
						"https://falabella.scene7.com/is/image/Falabella/8406270_2",
						"https://falabella.scene7.com/is/image/Falabella/8406270_3"));

		ProductoRequestDto request = new ProductoRequestDto();
		request.setBrand("Marca 2");
		request.setListUrls(urls);
		request.setName("Name 2");
		request.setPrice(new BigDecimal(200000));
		request.setSize("20");
		request.setSku("FAL-1000000");
		request.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_5");
		
		Producto producto = new Producto();
		producto.setBrand("Marca 1");
		producto.setListUrls(null);
		producto.setName("Name 1");
		producto.setPrice(new BigDecimal(100000));
		producto.setSize("30");
		producto.setSku("FAL-1000000");
		producto.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_4");
		
		Producto productoAGuardar = new Producto();
		productoAGuardar.setBrand("Marca 2");
		productoAGuardar.setListUrls(urls);
		productoAGuardar.setName("Name 2");
		productoAGuardar.setPrice(new BigDecimal(200000));
		productoAGuardar.setSize("20");
		productoAGuardar.setSku("FAL-1000000");
		productoAGuardar.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_5");
		
		ProductoResponseDto responseEsperado = new ProductoResponseDto();
		responseEsperado.setBrand("Marca 2");
		responseEsperado.setListUrls(urls);
		responseEsperado.setName("Name 2");
		responseEsperado.setPrice(new BigDecimal(100000));
		responseEsperado.setSize("20");
		responseEsperado.setSku("FAL-2000000");
		responseEsperado.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_5");
		
		Mockito.when(repository.findBySku(Mockito.anyString())).thenReturn(producto);
		Mockito.when(productoMapper.convertRequestToProductoDTO(Mockito.any())).thenReturn(productoAGuardar);
		Mockito.when(repository.save(Mockito.any())).thenReturn(productoAGuardar);
		Mockito.when(productoMapper.convertProductoToResponseDTO(Mockito.any())).thenReturn(responseEsperado);
		
		ProductoResponseDto resp = productosServiceImpl.updateProducto(request);
		assertNotNull(resp);
		assertEquals(responseEsperado, resp, "El producto actualizado, no corresponde con el esperado");
	}
	
	@Test
	public void testUpdateProductoException() {
		
		ProductoRequestDto request = new ProductoRequestDto();
		request.setBrand("Marca 2");
		request.setListUrls(null);
		request.setName("Name 2");
		request.setPrice(new BigDecimal(200000));
		request.setSize("20");
		request.setSku("FAL-1000000");
		request.setUrl("https://falabella.scene7.com/is/image/Falabella/8406270_5");
		
		Mockito.when(repository.findBySku(Mockito.anyString())).thenReturn(null);
		
		try {
			productosServiceImpl.updateProducto(request);
		} catch (ResourceNotFoundException e) {
			assertEquals("El producto no existe", e.getMessage());
		}
	}

}
