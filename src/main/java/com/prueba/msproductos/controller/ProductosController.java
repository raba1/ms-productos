package com.prueba.msproductos.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.msproductos.dto.ProductoRequestDto;
import com.prueba.msproductos.dto.ProductoResponseDto;
import com.prueba.msproductos.exception.ResourceNotFoundException;
import com.prueba.msproductos.service.ProductosService;

@RestController
@RequestMapping("/api/v1")
@Validated
public class ProductosController {
	
	@Autowired
	private ProductosService productosService;
	
	@PostMapping(value = "/productos")
	public ResponseEntity<ProductoResponseDto> save(@Valid @RequestBody ProductoRequestDto request) throws ResourceNotFoundException {
		return new ResponseEntity<>(productosService.save(request), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/productos")
	public ResponseEntity<List<ProductoResponseDto>> getProductos() throws ResourceNotFoundException{
		List<ProductoResponseDto> lista = productosService.getAllProductos();
		return new ResponseEntity<>(lista, HttpStatus.OK);
	} 
	
	@GetMapping(value = "/productos/{sku}")
	public ResponseEntity<ProductoResponseDto> getProducto(@PathVariable("sku") @Pattern(regexp="FAL-[0123456789]{7,8}", message = "El codigo sku no corresponde con el esperado") String sku) throws ResourceNotFoundException{
		ProductoResponseDto producto = productosService.getProducto(sku);
		return new ResponseEntity<>(producto, HttpStatus.OK);
	} 
	
	@DeleteMapping(value = "/productos/{sku}")
	public ResponseEntity<String> deleteProducto(@PathVariable("sku") @Pattern(regexp="FAL-[0123456789]{7,8}", message = "El codigo sku no corresponde con el esperado") String sku) throws ResourceNotFoundException{
		productosService.deleteProducto(sku);
		return new ResponseEntity<>(sku, HttpStatus.OK);
	} 
	
	@PutMapping(value = "/productos")
	public ResponseEntity<ProductoResponseDto> updateProducto(@Valid @RequestBody ProductoRequestDto request) throws ResourceNotFoundException{
		ProductoResponseDto productoResponseDto = productosService.updateProducto(request);
		return new ResponseEntity<>(productoResponseDto, HttpStatus.OK);
	}
	
}
