package com.prueba.msproductos.serviceImpl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.msproductos.dto.ProductoRequestDto;
import com.prueba.msproductos.dto.ProductoResponseDto;
import com.prueba.msproductos.entity.Producto;
import com.prueba.msproductos.exception.ResourceNotFoundException;
import com.prueba.msproductos.mapper.ProductoMapper;
import com.prueba.msproductos.repository.IProductoRepository;
import com.prueba.msproductos.service.ProductosService;

@Service
public class ProductosServiceImpl implements ProductosService {

	@Autowired
	private IProductoRepository repository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private ProductoMapper productoMapper;
	
	
	@Override
	public ProductoResponseDto save(ProductoRequestDto request) throws ResourceNotFoundException {
		Producto producto = repository.findBySku(request.getSku());
		if (producto != null) {
			throw new ResourceNotFoundException("El Sku del producto ya existe");
		}
		Producto productoGuardado = repository.save(this.mapeaProducto(request));
		return productoMapper.convertProductoToResponseDTO(productoGuardado);
	}
	
	@Override
	public List<ProductoResponseDto> getAllProductos() throws ResourceNotFoundException{
		List<Producto> lista = repository.findAll();
		if(lista.isEmpty()) {
			throw new ResourceNotFoundException("No existen productos");
		}
		return modelMapper.map(lista, List.class);
	}
	
	@Override
	public ProductoResponseDto getProducto(String sku) throws ResourceNotFoundException {
		ProductoResponseDto productoResponseDto = new ProductoResponseDto();
		Producto producto = repository.findBySku(sku);
		if (producto != null) {
			productoResponseDto = productoMapper.convertProductoToResponseDTO(producto);
		}else {
			throw new ResourceNotFoundException("El producto no existe");
		}
		return productoResponseDto;
	}
	
	@Override
	public void deleteProducto(String sku) throws ResourceNotFoundException {
		Producto producto = repository.findBySku(sku);
		if (producto != null) {
			repository.delete(producto);
		}else {
			throw new ResourceNotFoundException("El producto no existe");
		}
	}
	
	@Override
	public ProductoResponseDto updateProducto(ProductoRequestDto request) throws ResourceNotFoundException {
		ProductoResponseDto productoResponseDto = new ProductoResponseDto();
		Producto producto = repository.findBySku(request.getSku());
		if (producto != null) {
			producto = productoMapper.convertRequestToProductoDTO(request);
			repository.save(producto);
			productoResponseDto = productoMapper.convertProductoToResponseDTO(producto);
		}else {
			throw new ResourceNotFoundException("El producto no existe");
		}
		
		return productoResponseDto;
	}
	
	private Producto mapeaProducto(ProductoRequestDto request) {
		Producto producto = new Producto();
		producto.setBrand(request.getBrand());
		producto.setListUrls(request.getListUrls());
		producto.setName(request.getName());
		producto.setPrice(request.getPrice());
		producto.setSize(request.getSize());
		producto.setSku(request.getSku());
		producto.setUrl(request.getUrl());
		
		return producto;
	}

	
	
}
