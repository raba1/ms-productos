package com.prueba.msproductos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.prueba.msproductos.entity.Producto;

@Repository
public interface IProductoRepository extends JpaRepository<Producto, Long>{

	@Query(value = "SELECT * FROM PRODUCTO u WHERE u.sku = ?1", nativeQuery = true)
	Producto findBySku(String sku);
	
}
