package com.prueba.msproductos.entity;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Table(name = "PRODUCTO")
@Entity
public class Producto {
	
	@Id
	@Column(name = "SKU", unique = true, nullable = false)
	private String sku;
	
	@NotBlank
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	
	@NotBlank
	@Column(name = "brand", unique = false, nullable = false)
	private String brand;
	
	@NotBlank
	@Column(name = "size", unique = false, nullable = true)
	private String size;
	
	@Column(name = "price", unique = false, nullable = false)
	private BigDecimal price;
	
	@Column(name = "url", unique = false, nullable = false)
	private String url;
	
	@Column(name = "urls", unique = false, nullable = true)
	private ArrayList<String> listUrls;

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ArrayList<String> getListUrls() {
		return listUrls;
	}

	public void setListUrls(ArrayList<String> listUrls) {
		this.listUrls = listUrls;
	}
	
	
}
