package com.prueba.msproductos.service;

import java.util.List;

import com.prueba.msproductos.dto.ProductoRequestDto;
import com.prueba.msproductos.dto.ProductoResponseDto;
import com.prueba.msproductos.exception.ResourceNotFoundException;

public interface ProductosService {
	
	public ProductoResponseDto save(ProductoRequestDto request) throws ResourceNotFoundException ;
	
	public List<ProductoResponseDto> getAllProductos() throws ResourceNotFoundException;
	
	public ProductoResponseDto getProducto(String sku) throws ResourceNotFoundException;
	
	public void deleteProducto(String sku) throws ResourceNotFoundException;
	
	public ProductoResponseDto updateProducto(ProductoRequestDto request)  throws ResourceNotFoundException;

}
