package com.prueba.msproductos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductoRequestDto implements Serializable {

	private static final long serialVersionUID = -3618813956030901939L;
	
	@Pattern(regexp="FAL-[0123456789]{7,8}", message = "El codigo sku no corresponde con el esperado")
	private String sku;
	
	@NotBlank
	@Size(max=50,min=3,message="El nombre del producto no corresponde con el largo permitido")
	private String name;
	
	@NotBlank
	@Size(max=50,min=3,message="La marca del producto no corresponde con el largo permitido")
	private String brand;
	
	@NotBlank
	private String size;
	
	@DecimalMin(value = "1.00", inclusive = true, message ="El precio del producto no corresponde con el minimo permitido")
	@DecimalMax(value = "99999999.00", inclusive = true,  message ="El precio del producto no corresponde con el maximo permitido")
	private BigDecimal price;
	
	@Pattern(regexp="[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)", message = "La url no es valida")
	private String url;
	
	private ArrayList<String> listUrls;
	
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public ArrayList<String> getListUrls() {
		return listUrls;
	}
	public void setListUrls(ArrayList<String> listUrls) {
		this.listUrls = listUrls;
	}
	
	

}
