package com.prueba.msproductos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import lombok.Data;

@Data
public class ProductoResponseDto implements Serializable {

	private static final long serialVersionUID = -3618813956030901939L;

	private String sku;
	private String name;
	private String brand;
	private String size;
	private BigDecimal price;
	private String url;
	private ArrayList<String> listUrls;
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public ArrayList<String> getListUrls() {
		return listUrls;
	}
	public void setListUrls(ArrayList<String> listUrls) {
		this.listUrls = listUrls;
	}
	

}
