package com.prueba.msproductos.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prueba.msproductos.dto.ProductoRequestDto;
import com.prueba.msproductos.dto.ProductoResponseDto;
import com.prueba.msproductos.entity.Producto;

@Component
public class ProductoMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ProductoResponseDto convertProductoToResponseDTO(Producto producto) {
		return modelMapper.map(producto, ProductoResponseDto.class);
	}
	
	public Producto convertRequestToProductoDTO(ProductoRequestDto request) {
		return modelMapper.map(request, Producto.class);
	}
	
}
